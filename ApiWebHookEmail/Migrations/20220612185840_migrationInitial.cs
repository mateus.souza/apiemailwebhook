﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ApiWebHookEmail.Migrations
{
    public partial class migrationInitial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EMAIL_RESPONSE_API",
                columns: table => new
                {
                    ID_EMAIL = table.Column<string>(type: "text", nullable: false),
                    STATUS_EMAIL = table.Column<string>(type: "text", nullable: false),
                    DATA_ATUALIZACAO = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EMAIL_RESPONSE_API", x => x.ID_EMAIL);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EMAIL_RESPONSE_API");
        }
    }
}
