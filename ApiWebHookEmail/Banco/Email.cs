﻿using ApiWebHookEmail.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiWebHookEmail.Banco
{
    [Table("EMAIL_RESPONSE_API")]
    public class Email
    {
        [Column("ID_EMAIL")]
        public string Id { get; set; }

        [Column("STATUS_EMAIL")]
        public string Status { get; set; }

        [Column("DATA_ATUALIZACAO")]
        public DateTime DataAtualizacao { get; set; }

        public Email(string id, string status, DateTime dataAtualizacao)
        {
            Id = id;
            Status = status;
            DataAtualizacao = dataAtualizacao;
        }
        public Email() { }
    }

}
