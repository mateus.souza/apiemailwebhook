﻿using Microsoft.EntityFrameworkCore;

namespace ApiWebHookEmail.Banco
{
    public class EmailDbContext : DbContext
    {
        public DbSet<Email>? EmailStatuses { get; set; }
        public EmailDbContext(DbContextOptions options) : base(options) { }
    }
}
