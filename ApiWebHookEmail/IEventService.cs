using ApiWebHookEmail.Models;

namespace ApiWebHookEmail
{
    public interface IEventService
    {
          public IEnumerable<Event> GravaBanco(Stream stream);
    }
}