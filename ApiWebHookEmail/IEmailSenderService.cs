using ApiWebHookEmail.Models;

namespace ApiWebHookEmail
{
    public interface IEmailSenderService
    {
         public Task<string> EnviaEmailText(EmailText mailText);
    }
}