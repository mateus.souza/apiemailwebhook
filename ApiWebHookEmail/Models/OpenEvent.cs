﻿namespace ApiWebHookEmail.Models
{
    public class OpenEvent : Event
    {
        public string UserAgent { get; set; }  = null!;

        public string IP { get; set; }  = null!;
    }
}
