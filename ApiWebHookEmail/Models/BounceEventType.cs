﻿namespace ApiWebHookEmail.Models
{
    public enum BounceEventType
    {
        Bounce,
        Blocked,
        Expired
    }
}
