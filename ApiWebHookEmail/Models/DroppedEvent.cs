﻿namespace ApiWebHookEmail.Models
{
    public class DroppedEvent : Event
    {
        public string Reason { get; set; }  = null!;
        public string Status { get; set; }  = null!;
    }
}
