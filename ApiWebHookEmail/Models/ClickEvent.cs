﻿using ApiWebHookEmail.Converters;
using Newtonsoft.Json;
using System;

namespace ApiWebHookEmail.Models
{
    public class ClickEvent : OpenEvent
    {
        [JsonConverter(typeof(UriConverter))]
        public Uri Url { get; set; } = null!;
    }
}
