﻿namespace ApiWebHookEmail.Models
{
    public class DeliveredEvent : Event
    {
        public string Response { get; set; }  = null!;
    }
}
