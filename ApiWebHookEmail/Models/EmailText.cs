namespace ApiWebHookEmail.Models
{
    public class EmailText
    {
        public IFormFile? File { get; set; } = null!;
        public string NomeRemetente { get; set ;} = null!;
        public string NomeDestinatario { get; set; } = null!;
        public string EmailDestinatario { get; set; }  = null!;
        public string Assunto { get; set; } = null!;
        public string Mensagem { get ; set; } = null!;
    }
}