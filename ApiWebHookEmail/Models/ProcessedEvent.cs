﻿namespace ApiWebHookEmail.Models
{
    public class ProcessedEvent : Event
    {
        public Pool Pool { get; set; }  = null!;
    }
}
