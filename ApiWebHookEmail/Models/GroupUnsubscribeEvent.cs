﻿using Newtonsoft.Json;

namespace ApiWebHookEmail.Models
{
    public class GroupUnsubscribeEvent : ClickEvent
    {
        [JsonProperty("asm_group_id")]
        public int AsmGroupId { get; set; }
    }
}
