﻿

using ApiWebHookEmail.Converters;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ApiWebHookEmail.Models
{
    public class Event
    {
        public string Email { get; set; }  = null!;

        [JsonProperty("id_email")]
        public string IdEmail { get; set; } = null!;

        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime Timestamp { get; set; }

        [JsonProperty("smtp-id")]
        public string SmtpId { get; set; }  = null!;

        [JsonProperty("event")]
        [JsonConverter(typeof(StringEnumConverter))] 
        public EventType EventType { get; set; }

        [JsonConverter(typeof(CategoryConverter))]
        public Category Category { get; set; }  = null!;

        [JsonProperty("sg_event_id")]
        public string SendGridEventId { get; set; }  = null!;

        [JsonProperty("sg_message_id")]
        public string SendGridMessageId { get; set; }  = null!;

        public string TLS { get; set; }  = null!;

        [JsonExtensionData]
        public IDictionary<string, object> UniqueArgs { get; set; }  = null!;

        [JsonProperty("marketing_campaign_id")]
        public string MarketingCampainId { get; set; }  = null!;

        [JsonProperty("marketing_campaign_name")]
        public string MarketingCampainName { get; set; }  = null!;

    }
}
