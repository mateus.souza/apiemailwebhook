﻿using ApiWebHookEmail.Converters;
using ApiWebHookEmail.Models;
using ApiWebHookEmail.Parser;
using Microsoft.AspNetCore.Mvc;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace ApiWebHookEmail.Controllers
{
    [ApiController]
    [Route("[controller]")]

    public class EmailController : ControllerBase
    {
        private readonly IEventService _eventService;
        private readonly IEmailSenderService _emailSenderService;

        public EmailController(IEmailSenderService emailSenderService, IEventService eventService)
        {
            _emailSenderService = emailSenderService;
            _eventService = eventService;
        }

        [HttpPost]
        [Route("/send-message-mail")]
        public async Task<IActionResult> SendHtmlEmail([FromForm]EmailText mailtext)
        {
            return Ok(await _emailSenderService.EnviaEmailText(mailtext));
           
        }


        [Route("/events")]
        [HttpPost]
        public IActionResult Events()
        {
            IEnumerable<Event> events = _eventService.GravaBanco(Request.Body);
            return Ok(events);
        }
    }
}
