﻿using System.Collections.Immutable;
using ApiWebHookEmail.Banco;
using ApiWebHookEmail.Models;
using ApiWebHookEmail.Parser;

namespace ApiWebHookEmail
{
    public class EventService : IEventService
    {
        private readonly EmailDbContext _dbContext;

        public EventService(EmailDbContext dbContext)
        {
            _dbContext = dbContext;
        }


        public IEnumerable<Event> GravaBanco(Stream stream)
        {
            IEnumerable<Event> eventos = EventParser.ParseAsync(stream).Result;
            var emailsStatusesBanco = _dbContext.EmailStatuses!.ToImmutableList();

            foreach (Event evento in eventos)
            {
                try
                {
                    var mailById = GetEmailById(evento.IdEmail);
                    
                    if (mailById.Status != evento.EventType.ToString())
                    {
                        mailById.Status = evento.EventType.ToString();
                        mailById.DataAtualizacao = evento.Timestamp;
                        _dbContext.EmailStatuses!.Update(mailById);
                        _dbContext.SaveChanges();
                    }
                }
                catch (KeyNotFoundException e)
                {
                    var mails = new Email(evento.IdEmail, evento.EventType.ToString(), evento.Timestamp);
                    _dbContext.EmailStatuses!.Add(mails);
                    _dbContext.SaveChanges();
                }
            }

            return eventos;
        }

        private Email GetEmailById(string id)
        {
            return _dbContext.EmailStatuses!.Where(e => e.Id == id).FirstOrDefault() ?? throw new KeyNotFoundException();
        }

    }
}
