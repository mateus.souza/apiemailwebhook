using ApiWebHookEmail.Models;
using ApiWebHookEmail.Parser;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace ApiWebHookEmail
{
    public class EmailSenderService : IEmailSenderService
    {
        private readonly ISendGridClient _sendGridClient;
        private readonly IConfiguration _configuration;

        public EmailSenderService(ISendGridClient sendGridClient, IConfiguration configuration)
        {
            _sendGridClient = sendGridClient;
            _configuration = configuration;
        }

        public async Task<string> EnviaEmailText(EmailText mailText)
        {
            string getEmailRemetente = _configuration.GetSection("SendGridEmailSettings")
            .GetValue<string>("EmailRemetente");

            var mailRemetente = new EmailAddress(getEmailRemetente, mailText.NomeRemetente);
            var mailDestinatario = new EmailAddress(mailText.EmailDestinatario, mailText.NomeDestinatario);
            var assunto = mailText.Assunto;
            var mensagem = mailText.Mensagem;

            var msg = MailHelper.CreateSingleEmail(mailRemetente, mailDestinatario, assunto,
            mensagem, mensagem);

            //Adiciona argumento para identificar email via ID
            var argumento = "id_email";
            var id_email = Guid.NewGuid().ToString();
            msg.AddCustomArg(argumento, id_email);


            await msg.AddAttachmentAsync(
            mailText.File?.FileName,
            mailText.File?.OpenReadStream(),
            mailText.File?.ContentType,
            "attachment"
        );


            var response = await _sendGridClient.SendEmailAsync(msg);
            return response.IsSuccessStatusCode ? "E-mail enviado com sucesso, id: " + id_email : "Erro ao enviar e-mail: " + response.StatusCode; ;
        }
    }
}